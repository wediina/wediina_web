import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Router, RouterLinkWithHref } from '@angular/router';
import { getLocaleDateFormat } from '@angular/common';
import { Local } from 'protractor/built/driverProviders';
import { environment } from '../../environments/environment';
import { ConnectionService } from '../services/connection.service';
import { TabHeadingDirective } from 'ngx-bootstrap';

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  citynm: String;
  venueCategory = [];
  vendorCategory = [];
  cityarray = [];
  ca: String;
  user = false;
  userName: String;
  stateArray = [];
  vendorDropdown = false;
  venueDropdown = false;
  distArray = [];
  cat:String;
  vendorcat:String;
  vendors:String;
  venues:String;
  venuescat:String;
  calsit:String;
  vendor =[];
  vendorfilter =[];
  veid:String;
  venuefilter =[];
  venuesgrind =[];
  vcatId:String;
  constructor(private router: Router, private conectionservice: ConnectionService) { }

  ngOnInit() {
    if (localStorage.UserName != undefined || localStorage.UserName != null) {
      this.userName = localStorage.UserName;
      this.user = true;
    }
    $(document).ready(function () {
      $(".menu-icon").on("click", function () {
        $("nav ul").toggleClass("showing");
      });
    });

    console.log(localStorage.UserName);

    $(function() {
      $('input[name=chooseOption]').on('input',function() {
        var selectedOption = $('option[value="'+$(this).val()+'"]');
        console.log(selectedOption.length ? selectedOption.attr('id') : 'This opiton is not in the list!');
      });
    });


    window.onscroll = function () { scrollFunction() };

    function scrollFunction() {
      if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("gotoTop").style.display = "block";
      } else {
        document.getElementById("gotoTop").style.display = "none";
      }

    }

    // When the user clicks on the button, scroll to the top of the document

    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
    // $("script[src='assets/css/style.css']").remove();
    // $("script[src='node_modules/bootstrap/dist/css/bootstrap.min.css']").remove();
    // $("script[src='node_modules/jquery/dist/jquery.min.js']").remove();
    // $("script[src='node_modules/bootstrap/dist/js/bootstrap.min.js']").remove();

    // var dynamicScripts = [
    // "assets/css/style.css",
    // "node_modules/bootstrap/dist/css/bootstrap.min.css",
    // "node_modules/jquery/dist/jquery.min.js",
    // "node_modules/bootstrap/dist/js/bootstrap.min.js"
    // ];

    // for (var i = 0; i < dynamicScripts.length; i++) {
    //   let node = document.createElement('script');
    //   node.src = dynamicScripts[i];
    //   node.type = 'text/javascript';
    //   node.async = false;
    //   node.charset = 'utf-8';
    //   document.getElementsByTagName('head')[0].appendChild(node);
    // }

    // Scrolling Effect

    $(window).on("scroll", function () {
      if ($(window).scrollTop()) {
        $('.menu-icon').css("background", "#e2e2e2 !important");
        $('nav').addClass('black').css({ "box-shadow": "0px -1px 7px", "z-index": "3", "background": "#e2e2e2" });
        $('.city-menu').css("display", "block");
        $('.logo').addClass('logo-scroll');
        $('nav ul li a').css({ "color": "black", "transition": "0.5s", "text-shadow": "none" });
      }

      else {
        $('.logo').removeClass('logo-scroll');
        $('.menu-icon').removeAttr("style");

        $('nav').removeClass('black').removeAttr("style");
        $("nav ul li a").removeAttr("style");
        $(".city-menu").removeAttr("style");
        // $('').removeClass('text-black');

      }
    })

    this.getvendorCategory();
    this.getvenueCategory();
    this.getcity();
  }

  getcity() {
    this.conectionservice.getstatecity()
      .subscribe(res => {
        this.cityarray = res;
        this.cityarray.sort(function (a, b) {
          var textA = a.state.toUpperCase();
          var textB = b.state.toUpperCase();
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
        console.log(this.cityarray);

      });
  }


  getcityList(ca) {
    let calist = ca;
    let req = {
      state: calist
    }
    this.conectionservice.getstatecityList(req)
      .subscribe(res => {
        this.distArray = res[0].districts;
        this.distArray.sort(function (a, b) {
          var textA = a.vendor_cat_name.toUpperCase();
          var textB = b.vendor_cat_name.toUpperCase();
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
        console.log(res);

      });
  }



  openNav() {
    document.getElementById("mySidebarh").style.width = "325px";
    document.getElementById("mySidebarh").style.padding = "15px";
    // document.getElementById("mySidebarh").style.box-shadow = "7px -1px 12px 3px";

    // document.getElementById("overlay").style.width = "100%";
    //  document.getElementById("main").style.marginRight = "250px";
  }

  closeNav() {
    document.getElementById("mySidebarh").style.width = "0";
    document.getElementById("mySidebarh").style.padding = "0";
    // document.getElementById("overlay").style.width = "0";


    // document.getElementById("main").style.marginLeft= "0";
  }


  getcityName(c) {
    localStorage.setItem('cityName', c);
    environment.city = c;
this

  }

  getcategory(cetegory) {
    console.log(cetegory);

    if (cetegory == "vendor") {
      this.vendorDropdown = true;
      this.venueDropdown =false;
      // routeVendor(cat);
    }
    else {
      this.venueDropdown =true;
      this.vendorDropdown = false;

          }
  }


  routeVendor(ev){
    this.vendor =[];
  this.vendorfilter =[];

this.veid = ev.target.value;
    // this.spinner.show();
    this.conectionservice.getVendors()
      .subscribe(res => {
        // this.vendor = res;

        for (let index = 0; index < res.length; index++) {

          if (res[index].vendor_cat_id == this.veid) {

            if (res[index].prime_user == false && res[index].status == true) {
              this.vendor.push(res[index]);
              this.vendorfilter.push(res[index]);
              this.vendor.sort(function(a, b) {
                var textA = a.companyName.toUpperCase();
                var textB = b.companyName.toUpperCase();
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });
            this.vendor.sort(function(a, b) {
              var textA = a.companyName.toUpperCase();
              var textB = b.companyName.toUpperCase();
              return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
          });
            }
            // this.city.push(res[index].city);
          }
        }

        // this.city.forEach((item, index) => {
        //   if (index !== this.city.findIndex(i => i.name === item.name)) {
        //     this.city.splice(index, 1);
        //   }

        // });
        // this.city = this.city.filter((el, i, a) => i === a.indexOf(el))

        // console.log(this.vendor);
        // this.spinner.hide();
      });
  }



  routeVenueCat(ev) {
    this.venuefilter =[];
  this.venuesgrind =[];
    // this.spinner.show();
this.vcatId = ev.target.value;
    // location.reload();
    // window.history.replaceState({},'/Venues/'+this.id);
    this.conectionservice.getVenues()
      .subscribe(res => {
        console.log(res);

        // this.venues = res;
        // this.router.navigateByUrl('/Venues/'+this.id);
        for (let index = 0; index < res.length; index++) {
          if (res[index].venue_cat_id == this.vcatId ||  res[index].city == environment.city ) {
            if (res[index].status == true) {
              this.venuesgrind.push(res[index]);
              this.venuefilter.push(res[index]);
              this.venuesgrind.sort(function(a, b) {
                var textA = a.companyName.toUpperCase();
                var textB = b.companyName.toUpperCase();
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });
            this.venuefilter.sort(function(a, b) {
              var textA = a.companyName.toUpperCase();
              var textB = b.companyName.toUpperCase();
              return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
          });
            }
            // this.city.push(res[index].city);
          }
        }
        console.log("hello",this.venuesgrind)
        // this.city.forEach((item, index) => {
        //   if (index !== this.city.findIndex(i => i.name === item.name)) {
        //     this.city.splice(index, 1);
        //   }
        // });
        // this.city = this.city.filter((el, i, a) => i === a.indexOf(el))
        // console.log(this.venuefilter);
        // this.spinner.hide();

      });
  }
  topFunction() {

    $('html, body').animate({ scrollTop: 0 }, 'slow');
  }

  getvendorCategory() {
    this.conectionservice.getVendorCategory()
      .subscribe(res => {
        this.vendorCategory = res;
        this.vendorCategory.sort(function (a, b) {
          var textA = a.vendor_cat_name.toUpperCase();
          var textB = b.vendor_cat_name.toUpperCase();
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
        //console.log(this.vendorCategory, 'vnedor');

      });
  }


  getvenueCategory() {
    this.conectionservice.getVenueCategory()
      .subscribe(res => {
        this.venueCategory = res;
        this.venueCategory.sort(function (a, b) {
          var textA = a.venue_cat_name.toUpperCase();
          var textB = b.venue_cat_name.toUpperCase();
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
        //console.log(this.venueCategory, 'venue');

      });
  }

  userfetch() {
    if (localStorage.customer_id != null) {
      this.router.navigate(["/profile", localStorage.customer_id]);
    }

    else if (localStorage.venue_id != null) {
      this.router.navigate(["/Venueprofile", localStorage.venue_id]);
    }

    else if (localStorage.vendor_id != null) {
      this.router.navigate(["/Vendorprofile", localStorage.vendor_id]);
    }

    else {
      this.router.navigate(["/login"]);

    }
  }


  routeVendordetail(vvcat){
    let vvc =vvcat;
    this.router.navigate(["/VenderDetail/",vvc]);

  }

  routeVenuedetail(vvccat)
{
  let vvcc =vvccat;
  this.router.navigate(["/VenueDetail/",vvcc]);

}
  postCity() {
    this.stateArray = [{
      "state": "Chhattisgarh",
      "districts": [
        "Balod",
        "Baloda Bazar",
        "Balrampur",
        "Bastar",
        "Bemetara",
        "Bijapur",
        "Bilaspur",
        "Dantewada (South Bastar)",
        "Dhamtari",
        "Durg",
        "Gariyaband",
        "Janjgir-Champa",
        "Jashpur",
        "Kabirdham (Kawardha)",
        "Kanker (North Bastar)",
        "Kondagaon",
        "Korba",
        "Korea (Koriya)",
        "Mahasamund",
        "Mungeli",
        "Narayanpur",
        "Raigarh",
        "Raipur",
        "Rajnandgaon",
        "Sukma",
        "Surajpur  ",
        "Surguja"
      ]
    },
    {
      "state": "Dadra and Nagar Haveli (UT)",
      "districts": [
        "Dadra & Nagar Haveli"
      ]
    },
    {
      "state": "Daman and Diu (UT)",
      "districts": [
        "Daman",
        "Diu"
      ]
    },
    {
      "state": "Delhi (NCT)",
      "districts": [
        "Central Delhi",
        "East Delhi",
        "New Delhi",
        "North Delhi",
        "North East  Delhi",
        "North West  Delhi",
        "Shahdara",
        "South Delhi",
        "South East Delhi",
        "South West  Delhi",
        "West Delhi"
      ]
    },
    {
      "state": "Goa",
      "districts": [
        "North Goa",
        "South Goa"
      ]
    },
    {
      "state": "Gujarat",
      "districts": [
        "Ahmedabad",
        "Amreli",
        "Anand",
        "Aravalli",
        "Banaskantha (Palanpur)",
        "Bharuch",
        "Bhavnagar",
        "Botad",
        "Chhota Udepur",
        "Dahod",
        "Dangs (Ahwa)",
        "Devbhoomi Dwarka",
        "Gandhinagar",
        "Gir Somnath",
        "Jamnagar",
        "Junagadh",
        "Kachchh",
        "Kheda (Nadiad)",
        "Mahisagar",
        "Mehsana",
        "Morbi",
        "Narmada (Rajpipla)",
        "Navsari",
        "Panchmahal (Godhra)",
        "Patan",
        "Porbandar",
        "Rajkot",
        "Sabarkantha (Himmatnagar)",
        "Surat",
        "Surendranagar",
        "Tapi (Vyara)",
        "Vadodara",
        "Valsad"
      ]
    },
    {
      "state": "Haryana",
      "districts": [
        "Ambala",
        "Bhiwani",
        "Charkhi Dadri",
        "Faridabad",
        "Fatehabad",
        "Gurgaon",
        "Hisar",
        "Jhajjar",
        "Jind",
        "Kaithal",
        "Karnal",
        "Kurukshetra",
        "Mahendragarh",
        "Mewat",
        "Palwal",
        "Panchkula",
        "Panipat",
        "Rewari",
        "Rohtak",
        "Sirsa",
        "Sonipat",
        "Yamunanagar"
      ]
    },
    {
      "state": "Himachal Pradesh",
      "districts": [
        "Bilaspur",
        "Chamba",
        "Hamirpur",
        "Kangra",
        "Kinnaur",
        "Kullu",
        "Lahaul &amp; Spiti",
        "Mandi",
        "Shimla",
        "Sirmaur (Sirmour)",
        "Solan",
        "Una"
      ]
    },
    {
      "state": "Jammu and Kashmir",
      "districts": [
        "Anantnag",
        "Bandipore",
        "Baramulla",
        "Budgam",
        "Doda",
        "Ganderbal",
        "Jammu",
        "Kargil",
        "Kathua",
        "Kishtwar",
        "Kulgam",
        "Kupwara",
        "Leh",
        "Poonch",
        "Pulwama",
        "Rajouri",
        "Ramban",
        "Reasi",
        "Samba",
        "Shopian",
        "Srinagar",
        "Udhampur"
      ]
    },
    {
      "state": "Jharkhand",
      "districts": [
        "Bokaro",
        "Chatra",
        "Deoghar",
        "Dhanbad",
        "Dumka",
        "East Singhbhum",
        "Garhwa",
        "Giridih",
        "Godda",
        "Gumla",
        "Hazaribag",
        "Jamtara",
        "Khunti",
        "Koderma",
        "Latehar",
        "Lohardaga",
        "Pakur",
        "Palamu",
        "Ramgarh",
        "Ranchi",
        "Sahibganj",
        "Seraikela-Kharsawan",
        "Simdega",
        "West Singhbhum"
      ]
    },
    {
      "state": "Karnataka",
      "districts": [
        "Bagalkot",
        "Ballari (Bellary)",
        "Belagavi (Belgaum)",
        "Bengaluru (Bangalore) Rural",
        "Bengaluru (Bangalore) Urban",
        "Bidar",
        "Chamarajanagar",
        "Chikballapur",
        "Chikkamagaluru (Chikmagalur)",
        "Chitradurga",
        "Dakshina Kannada",
        "Davangere",
        "Dharwad",
        "Gadag",
        "Hassan",
        "Haveri",
        "Kalaburagi (Gulbarga)",
        "Kodagu",
        "Kolar",
        "Koppal",
        "Mandya",
        "Mysuru (Mysore)",
        "Raichur",
        "Ramanagara",
        "Shivamogga (Shimoga)",
        "Tumakuru (Tumkur)",
        "Udupi",
        "Uttara Kannada (Karwar)",
        "Vijayapura (Bijapur)",
        "Yadgir"
      ]
    },
    {
      "state": "Kerala",
      "districts": [
        "Alappuzha",
        "Ernakulam",
        "Idukki",
        "Kannur",
        "Kasaragod",
        "Kollam",
        "Kottayam",
        "Kozhikode",
        "Malappuram",
        "Palakkad",
        "Pathanamthitta",
        "Thiruvananthapuram",
        "Thrissur",
        "Wayanad"
      ]
    },
    {
      "state": "Lakshadweep (UT)",
      "districts": [
        "Agatti",
        "Amini",
        "Androth",
        "Bithra",
        "Chethlath",
        "Kavaratti",
        "Kadmath",
        "Kalpeni",
        "Kilthan",
        "Minicoy"
      ]
    },
    {
      "state": "Madhya Pradesh",
      "districts": [
        "Agar Malwa",
        "Alirajpur",
        "Anuppur",
        "Ashoknagar",
        "Balaghat",
        "Barwani",
        "Betul",
        "Bhind",
        "Bhopal",
        "Burhanpur",
        "Chhatarpur",
        "Chhindwara",
        "Damoh",
        "Datia",
        "Dewas",
        "Dhar",
        "Dindori",
        "Guna",
        "Gwalior",
        "Harda",
        "Hoshangabad",
        "Indore",
        "Jabalpur",
        "Jhabua",
        "Katni",
        "Khandwa",
        "Khargone",
        "Mandla",
        "Mandsaur",
        "Morena",
        "Narsinghpur",
        "Neemuch",
        "Panna",
        "Raisen",
        "Rajgarh",
        "Ratlam",
        "Rewa",
        "Sagar",
        "Satna",
        "Sehore",
        "Seoni",
        "Shahdol",
        "Shajapur",
        "Sheopur",
        "Shivpuri",
        "Sidhi",
        "Singrauli",
        "Tikamgarh",
        "Ujjain",
        "Umaria",
        "Vidisha"
      ]
    },
    {
      "state": "Maharashtra",
      "districts": [
        "Ahmednagar",
        "Akola",
        "Amravati",
        "Aurangabad",
        "Beed",
        "Bhandara",
        "Buldhana",
        "Chandrapur",
        "Dhule",
        "Gadchiroli",
        "Gondia",
        "Hingoli",
        "Jalgaon",
        "Jalna",
        "Kolhapur",
        "Latur",
        "Mumbai City",
        "Mumbai Suburban",
        "Nagpur",
        "Nanded",
        "Nandurbar",
        "Nashik",
        "Osmanabad",
        "Palghar",
        "Parbhani",
        "Pune",
        "Raigad",
        "Ratnagiri",
        "Sangli",
        "Satara",
        "Sindhudurg",
        "Solapur",
        "Thane",
        "Wardha",
        "Washim",
        "Yavatmal"
      ]
    },
    {
      "state": "Manipur",
      "districts": [
        "Bishnupur",
        "Chandel",
        "Churachandpur",
        "Imphal East",
        "Imphal West",
        "Jiribam",
        "Kakching",
        "Kamjong",
        "Kangpokpi",
        "Noney",
        "Pherzawl",
        "Senapati",
        "Tamenglong",
        "Tengnoupal",
        "Thoubal",
        "Ukhrul"
      ]
    },
    {
      "state": "Meghalaya",
      "districts": [
        "East Garo Hills",
        "East Jaintia Hills",
        "East Khasi Hills",
        "North Garo Hills",
        "Ri Bhoi",
        "South Garo Hills",
        "South West Garo Hills ",
        "South West Khasi Hills",
        "West Garo Hills",
        "West Jaintia Hills",
        "West Khasi Hills"
      ]
    },
    {
      "state": "Mizoram",
      "districts": [
        "Aizawl",
        "Champhai",
        "Kolasib",
        "Lawngtlai",
        "Lunglei",
        "Mamit",
        "Saiha",
        "Serchhip"
      ]
    },
    {
      "state": "Nagaland",
      "districts": [
        "Dimapur",
        "Kiphire",
        "Kohima",
        "Longleng",
        "Mokokchung",
        "Mon",
        "Peren",
        "Phek",
        "Tuensang",
        "Wokha",
        "Zunheboto"
      ]
    },
    {
      "state": "Odisha",
      "districts": [
        "Angul",
        "Balangir",
        "Balasore",
        "Bargarh",
        "Bhadrak",
        "Boudh",
        "Cuttack",
        "Deogarh",
        "Dhenkanal",
        "Gajapati",
        "Ganjam",
        "Jagatsinghapur",
        "Jajpur",
        "Jharsuguda",
        "Kalahandi",
        "Kandhamal",
        "Kendrapara",
        "Kendujhar (Keonjhar)",
        "Khordha",
        "Koraput",
        "Malkangiri",
        "Mayurbhanj",
        "Nabarangpur",
        "Nayagarh",
        "Nuapada",
        "Puri",
        "Rayagada",
        "Sambalpur",
        "Sonepur",
        "Sundargarh"
      ]
    },
    {
      "state": "Puducherry (UT)",
      "districts": [
        "Karaikal",
        "Mahe",
        "Pondicherry",
        "Yanam"
      ]
    },
    {
      "state": "Punjab",
      "districts": [
        "Amritsar",
        "Barnala",
        "Bathinda",
        "Faridkot",
        "Fatehgarh Sahib",
        "Fazilka",
        "Ferozepur",
        "Gurdaspur",
        "Hoshiarpur",
        "Jalandhar",
        "Kapurthala",
        "Ludhiana",
        "Mansa",
        "Moga",
        "Muktsar",
        "Nawanshahr (Shahid Bhagat Singh Nagar)",
        "Pathankot",
        "Patiala",
        "Rupnagar",
        "Sahibzada Ajit Singh Nagar (Mohali)",
        "Sangrur",
        "Tarn Taran"
      ]
    },
    {
      "state": "Rajasthan",
      "districts": [
        "Ajmer",
        "Alwar",
        "Banswara",
        "Baran",
        "Barmer",
        "Bharatpur",
        "Bhilwara",
        "Bikaner",
        "Bundi",
        "Chittorgarh",
        "Churu",
        "Dausa",
        "Dholpur",
        "Dungarpur",
        "Hanumangarh",
        "Jaipur",
        "Jaisalmer",
        "Jalore",
        "Jhalawar",
        "Jhunjhunu",
        "Jodhpur",
        "Karauli",
        "Kota",
        "Nagaur",
        "Pali",
        "Pratapgarh",
        "Rajsamand",
        "Sawai Madhopur",
        "Sikar",
        "Sirohi",
        "Sri Ganganagar",
        "Tonk",
        "Udaipur"
      ]
    },
    {
      "state": "Sikkim",
      "districts": [
        "East Sikkim",
        "North Sikkim",
        "South Sikkim",
        "West Sikkim"
      ]
    },
    {
      "state": "Tamil Nadu",
      "districts": [
        "Ariyalur",
        "Chennai",
        "Coimbatore",
        "Cuddalore",
        "Dharmapuri",
        "Dindigul",
        "Erode",
        "Kanchipuram",
        "Kanyakumari",
        "Karur",
        "Krishnagiri",
        "Madurai",
        "Nagapattinam",
        "Namakkal",
        "Nilgiris",
        "Perambalur",
        "Pudukkottai",
        "Ramanathapuram",
        "Salem",
        "Sivaganga",
        "Thanjavur",
        "Theni",
        "Thoothukudi (Tuticorin)",
        "Tiruchirappalli",
        "Tirunelveli",
        "Tiruppur",
        "Tiruvallur",
        "Tiruvannamalai",
        "Tiruvarur",
        "Vellore",
        "Viluppuram",
        "Virudhunagar"
      ]
    },
    {
      "state": "Telangana",
      "districts": [
        "Adilabad",
        "Bhadradri Kothagudem",
        "Hyderabad",
        "Jagtial",
        "Jangaon",
        "Jayashankar Bhoopalpally",
        "Jogulamba Gadwal",
        "Kamareddy",
        "Karimnagar",
        "Khammam",
        "Komaram Bheem Asifabad",
        "Mahabubabad",
        "Mahabubnagar",
        "Mancherial",
        "Medak",
        "Medchal",
        "Nagarkurnool",
        "Nalgonda",
        "Nirmal",
        "Nizamabad",
        "Peddapalli",
        "Rajanna Sircilla",
        "Rangareddy",
        "Sangareddy",
        "Siddipet",
        "Suryapet",
        "Vikarabad",
        "Wanaparthy",
        "Warangal (Rural)",
        "Warangal (Urban)",
        "Yadadri Bhuvanagiri"
      ]
    },
    {
      "state": "Tripura",
      "districts": [
        "Dhalai",
        "Gomati",
        "Khowai",
        "North Tripura",
        "Sepahijala",
        "South Tripura",
        "Unakoti",
        "West Tripura"
      ]
    },
    {
      "state": "Uttarakhand",
      "districts": [
        "Almora",
        "Bageshwar",
        "Chamoli",
        "Champawat",
        "Dehradun",
        "Haridwar",
        "Nainital",
        "Pauri Garhwal",
        "Pithoragarh",
        "Rudraprayag",
        "Tehri Garhwal",
        "Udham Singh Nagar",
        "Uttarkashi"
      ]
    },
    {
      "state": "Uttar Pradesh",
      "districts": [
        "Agra",
        "Aligarh",
        "Allahabad",
        "Ambedkar Nagar",
        "Amethi (Chatrapati Sahuji Mahraj Nagar)",
        "Amroha (J.P. Nagar)",
        "Auraiya",
        "Azamgarh",
        "Baghpat",
        "Bahraich",
        "Ballia",
        "Balrampur",
        "Banda",
        "Barabanki",
        "Bareilly",
        "Basti",
        "Bhadohi",
        "Bijnor",
        "Budaun",
        "Bulandshahr",
        "Chandauli",
        "Chitrakoot",
        "Deoria",
        "Etah",
        "Etawah",
        "Faizabad",
        "Farrukhabad",
        "Fatehpur",
        "Firozabad",
        "Gautam Buddha Nagar",
        "Ghaziabad",
        "Ghazipur",
        "Gonda",
        "Gorakhpur",
        "Hamirpur",
        "Hapur (Panchsheel Nagar)",
        "Hardoi",
        "Hathras",
        "Jalaun",
        "Jaunpur",
        "Jhansi",
        "Kannauj",
        "Kanpur Dehat",
        "Kanpur Nagar",
        "Kanshiram Nagar (Kasganj)",
        "Kaushambi",
        "Kushinagar (Padrauna)",
        "Lakhimpur - Kheri",
        "Lalitpur",
        "Lucknow",
        "Maharajganj",
        "Mahoba",
        "Mainpuri",
        "Mathura",
        "Mau",
        "Meerut",
        "Mirzapur",
        "Moradabad",
        "Muzaffarnagar",
        "Pilibhit",
        "Pratapgarh",
        "RaeBareli",
        "Rampur",
        "Saharanpur",
        "Sambhal (Bhim Nagar)",
        "Sant Kabir Nagar",
        "Shahjahanpur",
        "Shamali (Prabuddh Nagar)",
        "Shravasti",
        "Siddharth Nagar",
        "Sitapur",
        "Sonbhadra",
        "Sultanpur",
        "Unnao",
        "Varanasi"
      ]
    },
    {
      "state": "West Bengal",
      "districts": [
        "Alipurduar",
        "Bankura",
        "Birbhum",
        "Burdwan (Bardhaman)",
        "Cooch Behar",
        "Dakshin Dinajpur (South Dinajpur)",
        "Darjeeling",
        "Hooghly",
        "Howrah",
        "Jalpaiguri",
        "Kalimpong",
        "Kolkata",
        "Malda",
        "Murshidabad",
        "Nadia",
        "North 24 Parganas",
        "Paschim Medinipur (West Medinipur)",
        "Purba Medinipur (East Medinipur)",
        "Purulia",
        "South 24 Parganas",
        "Uttar Dinajpur (North Dinajpur)"
      ]
    }
    ]

    console.log(this.stateArray[0]);
    for (let index = 0; index < this.stateArray.length; index++) {

      let req = this.stateArray[index];
      this.conectionservice.postStateCity(req)
        .subscribe(res => {

        });
    }

  }



}


